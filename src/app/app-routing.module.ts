import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from './pages/products/products.component';


const routes: Routes = [
  { path: '',   redirectTo: 'productos', pathMatch: 'full' },
  { path: 'productos', component: ProductsComponent },
  { path: 'productos/:nombre', component: ProductsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
