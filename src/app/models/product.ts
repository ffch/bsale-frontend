export class Producto {
    id:         number;
    brand:      string;
    name:       string;
    name2:       string;
    price:      number;
    urlImage:   string;
    category:   string;
    dicount:    number;
}