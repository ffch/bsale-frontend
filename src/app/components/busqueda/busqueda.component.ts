import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Producto } from '../../models/product';
import { UtilsService } from '../../services/utils/utils.service';
import { ProductService } from '../../services/product/product.service';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.css']
})
export class BusquedaComponent implements OnInit {

  /* ----- Material autocomplete ----- */
  inputBusqueda = new FormControl();
  filteredOptions: Observable<string[]>;
  /* --------------------------------- */

  public nombreProductos:string[] = [];
  public productos: Producto[] = [];

  constructor(private productoService: ProductService, 
    private router:Router,
    private utils:UtilsService) { }

  ngOnInit(): void {
    
    this.utils.listaProductos$.subscribe( data => {
      this.productos= data;

      this.cargarProductos();
    });
    
  }

  /**
   * Método que se encarga de cargar los nombre de los productos 
   * para ser utilizados en la búsqueda
   */
  cargarProductos(){
    this.filteredOptions = this.inputBusqueda.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );

    this.nombreProductos = [];

    this.productos.forEach(producto => {
      this.nombreProductos.push(producto.name)
    });
  }

  /**
   * 
   * @param value Método que se encarga de filtrar los productos en la búsqueda
   */
  private _filter(value: string): string[] {

    if(value == null){
      return;
    }
    const filterValue = value.toLowerCase();

    return this.nombreProductos.filter((nombre: string) => nombre.toLowerCase().includes(filterValue));
  }

  /**
   * 
   * @param nombre 
   * 
   * Método que muestra un sólo producto al seleccionarlo en el matAutoComplete
   */
  mostrarProductoSeleccionado(nombre: string){
    this.router.navigate(['productos/' + nombre]);
  }
  
  /**
   * Método que muestra las coincidencias que se encuentran en el matAutoComplete
   */
  mostrarProductosEncontrados(){
    let nombre: string;
    nombre = this.inputBusqueda.value;
    if(nombre != null && nombre){
      this.router.navigate(['productos/' + nombre]);
    } else {
      this.utils.messageBad('Debe ingresar el nombre de un producto');
    }
  }

  /**
   * 
   * @param event 
   * 
   * Método que se encarga de ejecutar el método mostrarProductosEncontrados() cuando se
   * presiona la tecla ENTER
   */
  onKeydown(){
    this.mostrarProductosEncontrados();
  }
}
