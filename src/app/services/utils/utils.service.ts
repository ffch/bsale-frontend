import { Injectable, EventEmitter } from '@angular/core';
import {MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition} from '@angular/material/snack-bar'
import { NgxSpinnerService } from "ngx-spinner";
import { Producto } from '../../models/product';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  public horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  public verticalPosition: MatSnackBarVerticalPosition = 'top';
  public listaProductos$ = new EventEmitter<Producto[]>();

  constructor(private snackBar: MatSnackBar, private spinner: NgxSpinnerService) { }

  /**
   * Método que despliega el spinner en pantalla
   */
  showSpinner(){
    this.spinner.show(undefined, {
      type: "ball-square-clockwise-spin",
      size: "medium",
      bdColor: "rgba(0, 0, 0, 0.8)",
      color: "white"
    });
  }

  /**
   * Método que oculta el spinner
   */
  hideSpinner(){
    this.spinner.hide();
  }

  /**
   * 
   * @param message 
   * 
   * Método que despliega un mensaje de confirmación al usuario por pantalla 
   */
  messageGood(message: string): void {
		this.snackBar.open(message, 'x', {
			duration: 4000,
			horizontalPosition: this.horizontalPosition,
			verticalPosition: this.verticalPosition,
			panelClass: 'green-snackbar'

		});
  }
  
  /**
   * 
   * @param message 
   * 
   * Método que despliega un mensaje de error al usuario por pantalla 
   */
	messageBad(message: string): void {
		this.snackBar.open(message, 'x', {
			duration: 4000,
			horizontalPosition: this.horizontalPosition,
			verticalPosition: this.verticalPosition,
			panelClass: 'red-snackbar'

		});
  }

  /**
	 * 
	 * @param titulo 
	 * @param cantCaracteres 
	 * 
	 * Método que disminuye la cantidad de caracteres del parametro titulo
	 * segun el parametro cantCaracteres y agrega 3 puntos suspensivos al final de la cadena
	 */
	acortarString(titulo: string, cantCaracteres: number): string {
		let tituloCorto: string = '';

		for (let i = 0; i < cantCaracteres; i++) {

			tituloCorto += titulo[i];
			if (titulo[i + 1] == undefined) {
				tituloCorto += '...';
				return tituloCorto;
			}
		}

		tituloCorto += '...';

		return tituloCorto;
	}
}
