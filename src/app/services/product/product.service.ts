import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { config } from '../../../environments/environment.prod';
import { Producto } from '../../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private httpOption = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  constructor(private http: HttpClient) { }

  /**
   * Método que realiza una petición HTTP GET al servidor para obtener el listado de productos
   */
  obtenerProductos(): Observable<any>{

    return this.http.get<Producto[]>( config.API_URL_PRODUCTS , {observe: 'response', headers: this.httpOption});
  }
}
