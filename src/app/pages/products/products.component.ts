import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product/product.service';
import { ActivatedRoute } from '@angular/router';
import { Producto } from '../../models/product';
import { UtilsService } from '../../services/utils/utils.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  public nombreProductoBuscado:string = '';
  public ordenSeleccionado:string = '';
  public listaProductos:Producto[] = [];

  constructor(private productoService: ProductService, 
    private activatedRouter: ActivatedRoute,
    private utils: UtilsService) { }

  ngOnInit(): void {
    this.cargarProductos();
  }

  /**
   * Método que carga los productos desde el servidor y filtra la lista de productos
   * si se ha buscado un producto
   */
  cargarProductos(){
    this.activatedRouter.params.subscribe( params => {
      this.utils.showSpinner();

      this.nombreProductoBuscado = params.nombre;

      this.productoService.obtenerProductos().subscribe( data => {

        if(data.status == 200){
          this.listaProductos = data.body;

          this.listaProductos.forEach(pro => {

            if(pro.name.length > 27){

              pro.name2 = this.utils.acortarString(pro.name, 27);
            }
          });

          this.utils.listaProductos$.emit(this.listaProductos);

          if(this.nombreProductoBuscado != 'null' && this.nombreProductoBuscado != '' && this.nombreProductoBuscado != null){
            this.listaProductos = this.filtrarProductosPorNombre(this.nombreProductoBuscado);
          }
          
        }else{
          this.utils.hideSpinner();
          this.utils.messageBad("Error en el servidor, contáctese con el administrador.");
        }
        
        this.utils.hideSpinner();
      }, error => {
        console.log(error);
        this.utils.hideSpinner();
        this.utils.messageBad("Error en el servidor, contáctese con el administrador.");
      });

    }, error => {
      console.log(error);
      this.utils.hideSpinner();
      this.utils.messageBad("Error en el servidor, contáctese con el administrador.");
    });
    
  }

  /**
   * 
   * @param nombre 
   * 
   * Método que filtra los productos por el parámetro nombre
   */
  filtrarProductosPorNombre(nombre: string): Producto[]{

    let productosFiltrados:Producto[] = this.listaProductos.filter(product => product.name.toLowerCase().includes(nombre.toLowerCase()));

    if(productosFiltrados.length == 0){
      this.utils.messageBad("El producto " + nombre + " no existe en nuestro catálogo. Pruebe con otro nombre");
      return this.listaProductos;
    }
    return productosFiltrados;
  }

  /**
   * Método que ordena la lista de productos según lo seleccionado en el mat-select Ordenar por:
   * - nombre: alfabéticamente
   * - menormayor: precio de menor a mayor
   * - mayormenor: precio de mayor a menor
   */
  ordernarProductos(){

    if(this.ordenSeleccionado == "nombre"){
      this.listaProductos = this.listaProductos.sort((a,b) => (a.name.toLowerCase() > b.name.toLowerCase()) ? 1 : ((b.name.toLowerCase() > a.name.toLowerCase()) ? -1 : 0) );
    }else if(this.ordenSeleccionado == "menormayor"){
      this.listaProductos = this.listaProductos.sort((a,b) => a.price - b.price);
    }else if(this.ordenSeleccionado == "mayormenor"){
      this.listaProductos = this.listaProductos.sort((a,b) => b.price - a.price);
    }
   
  }
}
